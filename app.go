package main

/**
# ChatBotプロジェクト概要
# 開発環境：本番とローカル
#
#
#
#
#
*/
import (
	"database/sql"
	_ "database/sql"
	"encoding/base64"
	"encoding/json"
	"fmt"
	_ "fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	_ "strconv"
	"strings"

	_ "github.com/go-sql-driver/mysql"
	"github.com/ikawaha/kagome.ipadic/tokenizer"
	"github.com/joho/godotenv"
	"github.com/line/line-bot-sdk-go/linebot"
	"github.com/line/line-bot-sdk-go/linebot/httphandler"
	_ "github.com/ziutek/mymysql/godrv"
	"golang.org/x/net/context"
	_ "golang.org/x/text/encoding/japanese"
	_ "golang.org/x/text/transform"
	"google.golang.org/appengine"
	_ "google.golang.org/appengine/cloudsql"
	"google.golang.org/appengine/log"
	"google.golang.org/appengine/taskqueue"
	"google.golang.org/appengine/urlfetch"
	"regexp"

	"strconv"
	"time"

	"github.com/cloudflare/ahocorasick"
	"github.com/gorilla/sessions"
	_ "google.golang.org/appengine/datastore"
	"google.golang.org/appengine/memcache"
)

/************************************************
# COMMON DEFINITIONS
*************************************************/
var botHandler *httphandler.WebhookHandler

//var db *sql.DB
//var ctx context.Context
//Struct定義
type Message struct {
	Status string `json:"status"`
	Result string `json:"result"` //UPPERCASE is required
}
type Content struct {
	Text   string `json:"0"`
	Status string `json:"1"`
}
type MecabData struct {
	Status string   `json:"status"`
	Result []Result `json:"result"` //plural !!!
}

type Result struct {
	Surface   string `json:"surface"`
	Pos       string `json:"pos"`
	Pos1      string `json:"pos1"`
	Pos2      string `json:"pos2"`
	Pos3      string `json:"pos3"`
	Ctype     string `json:"form_type"`
	Cform     string `json:"form"`
	Origin    string `json:"origin"`
	Yomi      string `json:"yomi"`
	Pronounce string `json:"pronounce"`
}

type RunwayMessage struct {
	Status string `json:"status"`
	Result int    `json:"result"`
}

type User struct {
	UserId       string
	Username     string
	MessengerId  string
	CreatedTime  time.Time
	Mode         string
	ModifiedTime time.Time
}

type ChatLog struct {
	Id                     string
	UserId                 string
	GroupId                string
	UserMessenger          string
	BotMessenger           string
	UserMessageCreatedTime time.Time
	BotMessageCreatedTime  time.Time
}

type Category struct {
	Id           string
	CategoryName string
	CreatedTime  time.Time
	ModifiedTime time.Time
}

type MessageLabel struct {
	Id    string
	Label string
}

var store = sessions.NewCookieStore([]byte("aurorasystem"))

/************************************************
# GAE APP / MESSENGER BOT INITIALIZING
*************************************************/
func init() {
	//TODO: DETECT PRODUCTION OR DEVELOP MODE
	var err error
	if appengine.IsDevAppServer() != true {
		err = godotenv.Load("line.env", "database.env", "app.env")
		if err != nil {
			panic(err)
		}
	} else {
		err = godotenv.Load("line.dev.env", "database.dev.env")
		print("Dev mode")
		if err != nil {
			panic(err)
		}
	}

	botHandler, err = httphandler.New(
		os.Getenv("LINE_BOT_CHANNEL_SECRET"),
		os.Getenv("LINE_BOT_CHANNEL_TOKEN"),
	)
	botHandler.HandleEvents(handleCallback)

	http.Handle("/callback", botHandler)
	http.HandleFunc("/task", handleTask)

	//store.Options = &sessions.Options{
	//	Domain:   "gochatbot2016.appspot.com",
	//	Path:     "/",
	//	MaxAge:   3600 * 8, // 8 hours
	//	HttpOnly: false,
	//}
}
func newLINEBot(c context.Context) (*linebot.Client, error) {
	return botHandler.NewClient(
		linebot.WithHTTPClient(urlfetch.Client(c)),
	)
}

/************************************************
# REQUEST HANDLER
*************************************************/
// handleCallback is Webhook endpoint
func handleCallback(evs []*linebot.Event, r *http.Request) {
	c := newContext(r)
	ts := make([]*taskqueue.Task, len(evs))
	for i, e := range evs {
		j, err := json.Marshal(e)
		if err != nil {
			errorf(c, "json.Marshal: %v", err)
			return
		}
		data := base64.StdEncoding.EncodeToString(j)
		t := taskqueue.NewPOSTTask("/task", url.Values{"data": {data}})
		ts[i] = t
	}
	taskqueue.AddMulti(c, ts, "")
}

// handleTask is process event handler
func handleTask(w http.ResponseWriter, r *http.Request) {
	c := newContext(r)
	data := r.FormValue("data")
	if data == "" {
		errorf(c, "No data")
		return
	}

	j, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		errorf(c, "base64 DecodeString: %v", err)
		return
	}

	e := new(linebot.Event)
	err = json.Unmarshal(j, e)
	if err != nil {
		errorf(c, "json.Unmarshal: %v", err)
		return
	}
	//
	bot, err := newLINEBot(c)
	if err != nil {
		errorf(c, "newLINEBot: %v", err)
		return
	}

	// Get a session. We're ignoring the error resulted from decoding an
	// existing session: Get() always returns a session, even if empty.
/*	session, err := store.Get(r, e.Source.UserID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	logf(c, "SESSION VALUES UserId: %s, Mode: %s", session.Values["userid"], session.Values["mode"])
	// Set some session values.
	if session.Values["userid"] == nil {
		session.Values["userid"] = e.Source.UserID
	}

	if session.Values["groupid"] == nil {
		session.Values["groupid"] = e.Source.GroupID
	}
	if session.Values["mode"] == nil {
		session.Values["mode"] = "release"
	}*/
	//TODO: check if the user is in memcache
	var user User
	user.Mode = get_key_user(c,e.Source.UserID)
	logf(c,"get_key_user MODE: %v", user.Mode)
	if user.Mode == "" {
		logf(c,"save to memcached");
		user.UserId = e.Source.UserID
		//set key to memcache
		set_key_user(c, user.UserId, user)
		user.Mode = "release"
		var insertUserError = insertUser(c, user)
		if insertUserError == false {
			errorf(c,"insert User Error: %v",insertUserError)
		}
	}


/*
	//TODO: Cloud DataStore
	userDataStore := make([]User, 0, 10)
	//Initialize variable to hold the query returned data
	query := datastore.NewQuery("Users").Order("-Timestamp")
	keys, err2 := query.GetAll(c, &userDataStore)

	if err2 != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	//initialize variable to hold data for gui
	//need this so we can send Id and entity(Data) to template
	res := make([]ChatLog, 0, 10)
	for i, r := range userDataStore {
		k := keys[i]
		y := ChatLog{
			Id:     k,
			UserId: e.Source.UserID,
		}
		res = append(res, y)
	}
*/
	//userID := e.Source.UserID
	//groupID := e.Source.GroupID
	//RoomID := e.Source.RoomID
	var replyMessage string
	var replyType string
	logf(c, "EventType: %s\nMessage: %v", e.Type, e.Message)

	// Loop over array and print some stuff we found
	//for _, e1 := range mecabData.Result {
	//	logf("Origin: %v Pos: %v Pos1: %v \n", e1.Origin, e1.Pos, e1.Pos1)
	//}
	if e.Type == linebot.EventTypePostback {
		logf(c, "Postback data: %s", e.Postback.Data)
		var message_label MessageLabel
		message_label = parseMessageLabel(c, e.Postback.Data)
		insertMessageLabel(c, message_label)
		m := linebot.NewTextMessage("ラベルを付けました。普通モードを戻りたい場合「data off」を送ってください")
		if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
			errorf(c, "ReplyMessage: %v", err)
			return
		}
	}
	if e.Type == linebot.EventTypeMessage {
		switch message := e.Message.(type) {
		case *linebot.TextMessage:
			if caseInsenstiveContains(message.Text, "/help") {
				m := linebot.NewTextMessage("Command List:\n" +
					"data on -> ベル貼り付けモード\n" +
					"data off -> 本番モードに戻る\n" +
					"test on -> テストモード\n" +
					"/mode -> 現在のモード\n" +
					"/help -> ヘルプ")
				if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
					errorf(c, "ReplyMessage: %v", err)
					return
				}
			}
			if caseInsenstiveContains(message.Text, "/mode") {
				var mode_name string
				switch getUserMode(c, e.Source.UserID).Mode {
				case "data":
					mode_name = "ラベル貼り付"
					break
				case "test":
					mode_name = "テスト"
					break
				default:
					mode_name = "本番"
					break
				}
				m := linebot.NewTextMessage("現在のモードは" + mode_name + "です。")
				if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
					errorf(c, "ReplyMessage: %v", err)
					return
				}
			}
			if caseInsenstiveContains(message.Text, "data on") {
				//session.Values["mode"] = "data"
				updateUserMode(c, e.Source.UserID, "data")
				m := linebot.NewTextMessage("メッセージラベル貼り付けモードです。")
				if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
					errorf(c, "ReplyMessage: %v", err)
					return
				}
			}

			if caseInsenstiveContains(message.Text, "data off") {
				//session.Values["mode"] = "release"
				updateUserMode(c, e.Source.UserID, "release")
				m := linebot.NewTextMessage("本番モードです。")
				if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
					errorf(c, "ReplyMessage: %v", err)
					return
				}

			}

			if caseInsenstiveContains(message.Text, "test on") {
				//session.Values["mode"] = "test"
				updateUserMode(c, e.Source.UserID, "test")
				m := linebot.NewTextMessage("テストモードです。")
				if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
					errorf(c, "ReplyMessage: %v", err)
					return
				}

			}

			if caseInsenstiveContains(message.Text, "message_label") {
				var message_label MessageLabel
				message_label = parseMessageLabel(c, message.Text)
				insertMessageLabel(c, message_label)
				m := linebot.NewTextMessage("ラベルを付けました。普通モードを戻りたい場合「data off」を送ってください")
				if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
					errorf(c, "ReplyMessage: %v", err)
					return
				}

			}

			var user_mode = getUserMode(c, e.Source.UserID)
			logf(c, "MODE: %s", user_mode.Mode)

			switch user_mode.Mode {
			case "data":
				//TODO: save into database
				var message_id string
				message_id = insertMessageLog(c, e.Source.UserID, message.Text)
				template := linebot.NewButtonsTemplate(
					"", "仕事に関する質問ですか？", "下記の選択肢を選んでください",
					linebot.NewPostbackTemplateAction("仕事関係", "message_label_0_"+message_id, ""),
					linebot.NewPostbackTemplateAction("雑談", "message_label_1_"+message_id, ""),
				)

				m := linebot.NewTemplateMessage("PC版はまだ対応しておりません。", template)
				logf(c, "Reply Token: %s", e.ReplyToken)

				if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
					errorf(c, "Data Crawler ReplyMessage: %v", err)
					return
				}
				break
			case "test":
				imageURL := ""
				var message_id string
				message_id = insertMessageLog(c, e.Source.UserID, message.Text)
				template := linebot.NewButtonsTemplate(
					imageURL, "My button sample", "Hello, my button",
					linebot.NewURITemplateAction("Go to line.me", "https://line.me"),
					linebot.NewPostbackTemplateAction("仕事関係", "message_label_0_"+message_id, ""),
					linebot.NewPostbackTemplateAction("雑談", "message_label_1_"+message_id, ""),
					linebot.NewMessageTemplateAction("Say message", "Rice=米"),
				)
				if _, err := bot.ReplyMessage(
					e.ReplyToken,
					linebot.NewTemplateMessage("Buttons alt text", template),
				).Do(); err != nil {

				}
				break
			default:
				//system group special condition
				if e.Source.Type == linebot.EventSourceTypeGroup {
					logf(c, "EventSourceTypeGroup")

					if caseInsenstiveContains(message.Text, "phong") || strings.Contains(message.Text, "フォン") || strings.Contains(message.Text, "ふぉん") {
						logf(c, "Phong keyword")
						replyMessage, replyType = searchReplyMessage(c, message.Text)
						m := linebot.NewTextMessage(replyMessage)
						if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
							errorf(c, "ReplyMessage: %v", err)
							return
						}
					}
					return
				}

				//set replyMessage
				replyMessage, replyType = searchReplyMessage(c, message.Text)
				if replyType == "chat" {
					m := linebot.NewTextMessage(replyMessage)
					if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
						errorf(c, "ReplyMessage: %v", err)
						return
					}
				} else if replyType == "job" {
					leftBtn := linebot.NewMessageTemplateAction("みる", "left clicked")
					rightBtn := linebot.NewMessageTemplateAction("違いものを見たい", "right clicked")

					template := linebot.NewConfirmTemplate("コチラの案件はどうでしょうか？", leftBtn, rightBtn)

					m := linebot.NewTemplateMessage("Test", template)

					if _, err = bot.ReplyMessage(e.ReplyToken, m).WithContext(c).Do(); err != nil {
						errorf(c, "ReplyMessage: %v", err)
						return
					}
				}
				break

			}

		}
	}

	/*var messages []linebot.Message
	_ = append(messages,"test")
	_, err1 := bot.PushMessage(userID, messages...).Do()
	if err1 != nil {
		// Do something when some bad happened
	}*/
	//test connect with databse

	// Save it before we write to the response/return from the handler.
/*	session_save_err := session.Save(r, w)
	if session_save_err != nil {
		errorf(c, "Session Save Error %s", session_save_err)
	}
	logf(c, "SESSION SAVED VALUES UserId: %s, Mode: %s", session.Values["userid"], session.Values["mode"])*/

	w.WriteHeader(200)
}

//noinspection ALL
func insertMessageLog(c context.Context, user_id string, message string) string {
	db, _ := dbOpen(c)
	stmtIns, err := db.Prepare(fmt.Sprintf("INSERT INTO %s (user_id,user_message) VALUES (?, ?)", "chat_logs"))
	if err != nil {
		errorf(c, "DB ERROR")
	}
	defer stmtIns.Close() // Close the statement when we leave main() / the program terminates
	logf(c, "INSERT CHAT LOG: %s", message)
	res, err := stmtIns.Exec(user_id, message)
	db.Close()
	if err != nil {
		return "error"
	}

	id, _ := res.LastInsertId()
	return strconv.FormatInt(id, 10)
}
func parseMessageLabel(c context.Context, text string) MessageLabel {
	var messageLabel MessageLabel
	//message_label_label 0 1 _message_id
	if text[14:15] == "0" {
		messageLabel.Label = "job"
	} else {
		messageLabel.Label = "chatting"
	}
	messageLabel.Id = text[16:]
	logf(c, "Parse Message label: %s Id: %s", messageLabel.Label, messageLabel.Id)
	return messageLabel
}

/**
Insert Messenger User
*/
func insertUser(c context.Context, user User) bool {
	db, err := dbOpen(c)
	
	if err != nil {
		errorf(c, "DB ERROR %v",err)
		return false
	}
	stmtIns, err := db.Prepare(fmt.Sprintf("INSERT INTO %s (user_id,mode) VALUES (?, ?)", "users"))
	if err != nil {
		errorf(c, "DB ERROR %v",err)
		return false
	}
	defer stmtIns.Close() // Close the statement when we leave main() / the program terminates
	logf(c, "INSERT USER: %s", user.Mode)
	_, err = stmtIns.Exec(user.UserId, user.Mode)
	db.Close()
	if err != nil {
		errorf(c,"INSERT USER ERROR %v",err)
		return false
	}

	return true
}

/**
Insert Messenger User
*/
func insertMessageLabel(c context.Context, message MessageLabel) bool {
	db, _ := dbOpen(c)
	stmtIns, err := db.Prepare(fmt.Sprintf("INSERT INTO %s (message_id,message_label) VALUES (?, ?)", "message_label"))
	if err != nil {
		errorf(c, "DB ERROR")
	}
	defer stmtIns.Close() // Close the statement when we leave main() / the program terminates
	logf(c, "INSERT MESSAGE LABEL: %s", message.Id)
	_, err = stmtIns.Exec(message.Id, message.Label)
	db.Close()
	if err != nil {
		return false
	}

	return true
}

func updateUserMode(c context.Context, user_id string, mode string) bool {
	db, err := dbOpen(c)
	if err != nil {
		errorf(c, "DB ERROR %v",err)
	}
	stmtIns, err := db.Prepare(fmt.Sprintf("UPDATE %s SET mode = ? WHERE ( user_id = ?)", "users"))
	if err != nil {
		errorf(c, "DB ERROR %v",err)
	}
	defer stmtIns.Close() // Close the statement when we leave main() / the program terminates
	logf(c, "UPDATE USER MODE: %s", mode)
	_, err = stmtIns.Exec(mode, user_id)
	db.Close()
	if err != nil {
		return false
	}
	var user User
	user.Mode = mode
	set_key_user(c,user_id,user)
	return true
}

func getUserMode(c context.Context, user_id string) User {
	//TODO: IF MEMCACHED NOT EXISTED => GET FROM SQL
	var user User
	user.UserId = user_id
	user.Mode = get_key_user(c,user_id);
	if user.Mode == "" {
		db, _ := dbOpen(c)
		stmtOut, err := db.Prepare(fmt.Sprintf("SELECT mode FROM %s WHERE user_id = ? LIMIT 1", "users"))
		if err != nil {
			panic(err.Error())
		}
		defer stmtOut.Close()
		var user User
		var mode string
		if err := stmtOut.QueryRow(user_id).Scan(&mode); err != nil {
			return user
		}
		db.Close()
		user.Mode = mode
		logf(c, "GETUSERMODE: %s", mode)
		return user
	}
	return user
}

/************************************************
# SEND RECEIVE HANDLER (USER SESSION HANDLER)
*************************************************/
/**
SQL接続トライ
*/
func searchReplyMessage(ctx context.Context, message string) (string, string) {
	var keywords = patternFilter(ctx, message)
	var sourceFlg = simpleSwitchAnswerSource(ctx, message)
	if sourceFlg == 1 {
		var runwayResult = runwayApi(ctx, keywords)
		logf(ctx, "Runway AnkenId: ", strconv.Itoa(runwayResult))
		if runwayResult != -1 {
			var runwayAnkenUrl = "http://run-way.jp/kigyou_anken/index/" + strconv.Itoa(runwayResult) + "/"
			return "コチラの案件はどうでしょうか？\n" + runwayAnkenUrl, "job"
		} else {
			return getMessageFromLocalUser(ctx, message), "chat"
		}
	} else if sourceFlg == 2 {
		if len(keywords) > 0 {
			db, err := sql.Open("mysql", "root:phong@cloudsql(gochatbot2016:us-central:phong-mysql-first-gen)/phong")
			var queryLikeString string
			for i := range keywords {
				orString := ""
				if i+1 < len(keywords) {
					orString = " OR "
				}
				queryLikeString += "keyword LIKE '%" + keywords[i] + "%'" + orString
				//fmt.Println(keywords[i])
			}
			rows, err := db.Query("SELECT message, priority FROM dictionary WHERE " + queryLikeString)
			var s string
			if err != nil {
				errorf(ctx, "db.Query: %v", err)
				return getMessageFromLocalUser(ctx, message), "job"
			}
			defer rows.Close()

			for rows.Next() {
				var message string
				var priority string
				if err := rows.Scan(&message, &priority); err != nil {
					errorf(ctx, "rows.Scan: %v", err)
					continue
				}
				log.Infof(ctx, "Message: %v - Priority: %v", message, priority)
				s = message
				break
			}
			if err := rows.Err(); err != nil {
				errorf(ctx, "Row error: %v", err)
			}
			db.Close()
			if s != "" {
				return "コチラの案件はどうでしょうか？\n" + s, "chat"
			} else {
				return getMessageFromLocalUser(ctx, message), "chat"
			}
		}
	}
	return getMessageFromLocalUser(ctx, message), "chat"
}

/************************************************
# TEXT PROCESSING
*************************************************/

/************************************************
# TEXT CLASSIFIER
*************************************************/
/**
filter common keyword
*/
func patternFilter(ctx context.Context, message string) []string {
	var stringSlide []string
	var mecabData MecabData
	userLocalSentenceDecompose(ctx, message, "true", false, &mecabData)
	logf(ctx, "Result: %s", mecabData.Status)
	if mecabData.Status != "success" {
	} else {
		// Loop over array and print some stuff we found
		stringSlide = setMecabCategory(ctx, mecabData)
		//fmt.Println("xxxx"+mecabData.Result[0].Pos1+"ssssss")
	}

	//TODO: query all common keywords (meaningless) and remove all from message (or using mecab lib)
	//stringSlice[0] = strings.Replace(stringSlice[0], "を探したいです","",-1)
	//sql query
	return stringSlide
}

/**
Pattern Filter
return 1 => runway 2=>dictionary -1 => userlocal
*/
func simpleSwitchAnswerSource(ctx context.Context, message string) int {
	var aType int = -1
	//TODO: if message include some patterns below
	patterns := []string{
		"仕事", "探", "教え", "欲しい",
		"相談", "未経験", "高時給", "金", "しごと", "おしえ",
	}

	m := ahocorasick.NewStringMatcher(patterns)

	found := m.Match([]byte(message))
	logf(ctx, "found patterns", found)
	//set threshold = 2 if two keywords be found then type = 1
	if len(found) >= 2 {
		aType = 1
	}
	//dictionary

	return aType
}

/************************************************
# API CONNECT
*************************************************/
func getMessageFromLocalUser(ctx context.Context, message string) string {
	logf(ctx, "Function: getMessageFromLocalUser")
	var requestUri = "https://chatbot-api.userlocal.jp/api/chat?key=6208b30b87806d082777&message=" + message
	var replyMessage string = ""

	received_message := new(Message)
	var log_err = getJson(ctx, requestUri, received_message)
	if log_err != nil {
		errorf(ctx, "UserLocal error: %v", log_err)
	}
	if received_message.Status == "success" {
		replyMessage = received_message.Result
	} else {
		replyMessage = "考え中です。"
	}

	return replyMessage

}

/**
Run-way Data Retrieve Api
*/
func runwayApi(ctx context.Context, keywords []string) int {
	var keyword_params string = ""
	for i := range keywords {
		keyword_params += keywords[i] + "/"
	}
	var requestUri = "http://preview.run-way.jp/chatbot_api/get_reccomend_anken_id/" + keyword_params

	received_message := new(RunwayMessage)

	var err = getJsonHttp(ctx, requestUri, received_message)
	if err != nil {
		//TODO: print error log
		errorf(ctx, "Runway error: %v", err)
	}

	logf(ctx, "Runway Result: %s", received_message.Result)
	return received_message.Result
}

/**
Not an appengine context so split it into 2 case (local and appengine)
*/

func userLocalSentenceDecompose(ctx context.Context, message string, detailFlg string, localFlg bool, target interface{}) error {
	var requestUri string
	//var err1 error
	//if localFlg == true {
	//requestUri = "https://chatbot-api.userlocal.jp/api/decompose?message="+message+"&key=6208b30b87806d082777&detail="+detailFlg;
	//fmt.Println(requestUri)
	//resp, err := http.Get(requestUri)
	//if err != nil {
	//	// handle error
	//}
	//defer resp.Body.Close()
	//
	//body, err := ioutil.ReadAll(resp.Body)
	//fmt.Println("==============")
	//fmt.Println(string(body))
	//fmt.Println("==============")
	//
	////var d MecabData
	////json.Unmarshal(body, target)
	////fmt.Printf("%+v\n", d)

	//return json.NewDecoder(resp.Body).Decode(target)
	//} else {
	requestUri = "https://chatbot-api.userlocal.jp/api/decompose?message=" + message + "&key=6208b30b87806d082777&detail=" + detailFlg

	var err = getJson(ctx, requestUri, target)
	if err != nil {
		//TODO: print error log

	}
	return err
	//}
	//return err1
}

/************************************************
# COMMON SUPPORT FUNCTIONS
*************************************************/
func logf(c context.Context, format string, args ...interface{}) {
	log.Infof(c, format, args...)
}

func errorf(c context.Context, format string, args ...interface{}) {
	log.Errorf(c, format, args...)
}

func newContext(r *http.Request) context.Context {
	return appengine.NewContext(r)
}

//Support functions
func caseInsenstiveContains(a, b string) bool {
	return strings.Contains(strings.ToUpper(a), strings.ToUpper(b))
}

func getJson(c context.Context, url string, target interface{}) error {
	client := urlfetch.Client(c)
	r, err := client.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

/**
MECABデータをカテゴリーに分類する
*/
func setMecabCategory(ctx context.Context, mecabData MecabData) []string {
	var array []string
	for _, e := range mecabData.Result {
		switch e.Pos {
		case "名詞":
			array = append(array, e.Surface)
			logf(ctx, "Noun: %s", e.Surface)
			break
		case "動詞":
			logf(ctx, "Verb: %s", e.Origin)
			break
		}

	}
	return array
}

func commonChatMessageFilter() string {
	var message string
	message = ""
	return message
}

func textProcessing() []string {
	var strings []string

	return strings
}

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}
func getJsonHttp(c context.Context, url string, target interface{}) error {
	client := urlfetch.Client(c)
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Add("Authorization", "Basic "+basicAuth("systemuser", "2R6UitjCA5"))

	resp, err := client.Do(req)
	if err != nil {
		logf(c, "getJsonError %s", err)
	}
	//bodyText, err := ioutil.ReadAll(resp.Body)
	//logf(c,"getJsonHttpBody %s",resp)
	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(target)
}
func sendPost(c context.Context, uri string, message string, target interface{}) error {
	hc := http.Client{}
	form := url.Values{}
	form.Add("mecabText", message)
	req, err := http.NewRequest("POST", uri, strings.NewReader(form.Encode()))
	if err != nil {
		return err
	}
	req.PostForm = form
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	fmt.Println(form)
	resp, err2 := hc.Do(req)
	if err2 != nil {
		return err2
	}

	// Bodyを読み込む
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err.Error())
		return err
	}
	// ステータスによるエラーハンドリング
	if resp.StatusCode != http.StatusOK {
		fmt.Printf("%s", body)
		return err
	}
	fmt.Printf("%s", body)
	jsonStr := `{"afasdfsad","asdlfasdf"}`
	body = []byte(jsonStr)
	// BodyのJSONをデコードする
	return json.Unmarshal(body, target)

	//body, err := ioutil.ReadAll(resp.Body)

	//fmt.Println(string(body))
	//// --- Decoding: convert encS from ShiftJIS to UTF8
	//// declare a decoder which reads from the string we have just encoded
	//rInUTF8 := transform.NewReader(strings.NewReader(string(body)), japanese.EUCJP.NewDecoder())
	//// decode our string
	//decBytes, _ := ioutil.ReadAll(rInUTF8)
	//decS := string(decBytes)
	//fmt.Println(decS)
	//if err != nil {
	//	//panic()
	//	fmt.Println("error")
	//}
	//fmt.Println("==================")
	//body2, _ := ioutil.ReadAll(resp.Body)
	////defer resp.Body.Close()
	//fmt.Println(string(body2))
	//var objmap map[string]*json.RawMessage
	//buf, err := ioutil.ReadAll(resp.Body)
	//if err != nil {
	//	return err
	//}
	//err2 := json.Unmarshal(buf, &objmap)
	//if err2 != nil {
	//	fmt.Println("err22")
	//}
	//fmt.Print(objmap)
	//return json.Unmarshal(body, target)
	//return json.NewDecoder(resp.Body).Decode(target)
	//defer resp.Body.Close()

}

func sentenceExtracter(ctx context.Context, sentence string) {
	//var content Content
	t := tokenizer.New()
	tokens := t.Tokenize(sentence) // t.Analyze("寿司が食べたい。", tokenizer.Normal)
	for _, token := range tokens {
		if token.Class == tokenizer.DUMMY {
			// BOS: Begin Of Sentence, EOS: End Of Sentence.
			//fmt.Printf("%s\n", token.Surface)
			log.Infof(ctx, "%s\n", token.Surface)
			continue
		}
		features := strings.Join(token.Features(), ",")
		log.Infof(ctx, "%s\t%v\n", token.Surface, features)
		//content.Pos = token.Features()
		//fmt.Printf("%s\t%v\n", token.Surface, features)
	}
	//return content
}

/*GO PRACTICE------------------------------------*/
/**
/**
for test on localhost
*/
/*
func main() {
	//println("this is a test");
	i := 10
	fmt.Println(i)
	fmt.Println(&i)

	var p1 *int
	fmt.Println(p1)
	p1 = &i
	p2 := &i
	fmt.Println(p1)
	fmt.Println(p2)
	fmt.Println(&p1)

	var p11 = new(int)
	fmt.Println(p11)
	fmt.Println(*p11)
	*p11 = 10
	fmt.Println(*p11)

	//array
	fmt.Println("Array Test")
	fmt.Println("----------")
	var a1 []int
	var a2 []int = []int{1,2,3}
	a3 := make([]int, 3)
	a4 := []int{1:1,3:2}
	a5 := make([]int, 3, 5)
	fmt.Println(a1)
	fmt.Println(a2)
	fmt.Println(a3)
	fmt.Println(a4)
	fmt.Println(a5)

	fmt.Println("Test mysql")
	testMySQL()
}

func testMySQL() {
	db, err := sql.Open("mysql","root:@tcp(127.0.0.1:3306)/test?charset=utf8")
	if err != nil {
		fmt.Println("error")
	} else {
		fmt.Println("connected")
	}

	result, err := db.Exec("INSERT entrances SET name = 'phong'")
	if err != nil {
		fmt.Println("error insert into table")
	}
	id, err := result.LastInsertId()
	fmt.Print(id)
	if err != nil {
		//fmt.Print(id)
		//fmt.Println("Inserted Id ="+strconv.Itoa(int(id)))
	}
	defer db.Close()
}
*/
func failOnError(err error) {
	if err != nil {
		fmt.Errorf("Error:", err)
	}
}

/*func main() {
//type Message struct {
//	Status string
//	Result string
//}
//var test_url = "https://chatbot-api.userlocal.jp/api/chat?key=6208b30b87806d082777&message=こんばんは"
////test_url = "https://phong.jp"
//message := new(Message)
//var log = getJson(test_url, message)
//fmt.Println(log)
//fmt.Println(message.Result)

//type Content struct {
//	Pos, Pos1, Pos2, Pos3,
//	Katuyougata, Katuyoukei, Kihonkei,
//	Yomi, Pronunciation string
//}
//t := tokenizer.New()
//tokens := t.Tokenize("寿司が食べたい。") // t.Analyze("寿司が食べたい。", tokenizer.Normal)
//for _, token := range tokens {
//	if token.Class == tokenizer.DUMMY {
//		// BOS: Begin Of Sentence, EOS: End Of Sentence.
//		fmt.Printf("%s\n", token.Surface)
//		continue
//	}
//	features := strings.Join(token.Features(), ",")
//	fmt.Printf("%s\t%v\n", token.Surface, features)
//}
*/ /**
sampleData id, sentence, word, category, weight_value, priority
machine learning, deep learning algorithms
get question response data -> training -> flag weight value -> modeling -> apple model to new data -> predict (find appropriate response)
*/ /*

	ctx := context.Background()
	var url = "http://192.168.11.56/mecab.php"
	var message = "エキサイト"

	//mecabData := new(MecabData)
	var mecabData MecabData
	userLocalSentenceDecompose(ctx, message, "true", true, &mecabData)
	fmt.Println("Result: " + mecabData.Status)
	// Loop over array and print some stuff we found
	for _, e := range mecabData.Result {
		fmt.Printf("Origin: %v Pos: %v Pos1: %v \n", e.Origin, e.Pos, e.Pos1)
	}
	//fmt.Println("xxxx"+mecabData.Result[0].Pos1+"ssssss")

	//var mecab Mecab
	var content = Content{}
	sendPost(ctx, url, message, &content)
	//t := reflect.TypeOf(Content{})
	fmt.Println(content)
	//content2 := Content{"aaa","asfs"}
	//b, _ := json.Marshal(content2)
	//fmt.Println(string(b))

	//var sampleData []string
	//for i := 0; i <= 100; i++ {
	//	sampleData = append(sampleData, "エキサイトニュースは、気になる事件の速報や話題の芸能ニュースをピックアップ。")
	//}
	//fmt.Println(sampleData[2])
	//template base question-answer or generate answer

	//csv read write
	flag.Parse()

	//読み込みファイル準備
	file1, err := os.Open(flag.Arg(0))
	failOnError(err)
	defer file1.Close()

	//書き込みファイル準備
	file2, err := os.Create(flag.Arg(1))
	failOnError(err)
	defer file2.Close()

	//reader := csv.NewReader(transform.NewReader(file1, japanese.ShiftJIS.NewDecoder()))
	//  reader := csv.NewReader(transform.NewReader(file1, japanese.EUCJP.NewDecoder()))
	  reader := csv.NewReader(file1) //utf8

	//writer := csv.NewWriter(transform.NewWriter(file2, japanese.ShiftJIS.NewEncoder()))
	//  writer := csv.NewWriter(transform.NewWriter(file2, japanese.EUCJP.NewEncoder()))
	  writer := csv.NewWriter(file2) //utf8
	//writer.UseCRLF = true //デフォルトはLFのみ
	//  writer.Comma = '\t'

*/ /*Test json*/ /*
	var jsonStr = `{
	    "Name": "Ken", "Age": 24
	  }`
	type User struct {
		Name string
		Age  uint16
	}
	var u User
	json.Unmarshal([]byte(jsonStr), &u)
	fmt.Println(u) // {Ken 24}
	fmt.Println("Start")
	for {
		record, err := reader.Read() // 1行読み出す
		if err == io.EOF {
			break
		} else {
			failOnError(err)
		}
		var new_record []string
		for i, v := range record {
			if i > 0 {
				new_record = append(new_record, fmt.Sprint(i) + ":" + v)
			}
		}
		writer.Write(new_record) // 1行書き出す
		//      log.Printf("%#v", record[0] + "," + record[1])
	}
	writer.Flush()
	fmt.Println("Finish !")
}*/

func weightLikelihood() {
	type Sentence struct {
		Pattern string // abc {_xyz_} alsjdflasdjf {_asldjf_} alsdjfals;
		Info    string
	}
	type Bot struct {
		age       int
		gender    string
		character string
	}
	//TODO: get answer value from user
	//TODO: split words and classify to category
	//TODO:
	//TODO: insert value into database/text file
	//TODO: update value to db/txt

}

func matchSentencePattern() bool {
	var sentence1 string
	//sentence2, sentence3, sentence4
	//sentence1 = "What is a CEO"
	//sentence2 = "What is a geisha?"
	//sentence3 = "What is ``R.E.M.``?"
	//sentence4 = "What's SCUBA?"

	//c1 = re.compile(r"^[wW]hat(?: is| are| was| were|\'s)(?: a| an| the)? [`']{0,2}((?:[A-Z]\.)+|[A-Z]+)[`']{0,2} ?\??")

	if m, _ := regexp.MatchString("^[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}$", sentence1); !m {
		return false
	}
	return true
}
/**
MEMCACHE
 */
func key(k string) string {
	s := []string{k, "clipboard"}
	return strings.Join(s, "_")
}

func set_key_user(c context.Context, k string, v User) {

	item := &memcache.Item{
		Key:   key(k),
		Value: []byte(v.Mode),
		Object: &v,
	}

	if err := memcache.Add(c, item); err == memcache.ErrNotStored {
		if err := memcache.Set(c, item); err != nil {
			log.Errorf(c,"error setting item: %v", err)
		}
	} else if err != nil {
		log.Errorf(c,"error adding item: %v", err)
	}

	logf(c,"the value is %q", item.Object)
}

func get_key_user(c context.Context, k string) string {
	if item, err := memcache.Get(c, key(k)); err == memcache.ErrCacheMiss {
		logf(c,"item not in the cache")
		return ""
	} else if err != nil {
		errorf(c,"error getting item: %v", err)
		return ""
	} else {
		return string(item.Value)
	}
}
/**
Database functions
*/
func dbOpen(ctx context.Context) (*sql.DB, error) {
	//TODO: Open Database connection
	connectionName := mustGetenv(ctx, "CLOUDSQL_CONNECTION_NAME")
	user := mustGetenv(ctx, "CLOUDSQL_USER")
	password := os.Getenv("CLOUDSQL_PASSWORD") // NOTE: password may be empty
	databaseName :=mustGetenv(ctx, "CLOUDSQL_DATABASE_NAME")
	logf(ctx, "CONNECTION NAME: %v", connectionName)
	db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@cloudsql(%s)/%s", user, password, connectionName,databaseName))
	return db, err
}

func dbClose() {
	//TODO: Close Database connection
}

func mustGetenv(ctx context.Context, k string) string {
	v := os.Getenv(k)
	if v == "" {
		errorf(ctx, "%s environment variable not set.", k)
	}
	return v
}
